require 'spec_helper'

RSpec.describe HtmlRenderer do
  before do
    @store = Store.create name: 'Test Store', email: 'test@example.com'
    @subs1 = Subscriber.create store: @store, email: 'pgpadron@gmail.com', first_name: 'Pedro', last_name: 'Padron'
    @subs2 = Subscriber.create store: @store, email: 'marcos@smartrmail.com', first_name: 'Marcos', last_name: 'Teixeira'
    @order1 = Order.create store: @store, subscriber: @subs1, total: 30
    @order2 = Order.create store: @store, subscriber: @subs1, total: 20
  end

  it 'supports the last_order_amount variable' do
    mailing = Mailing.create store: @store, html: 'You spent {{ last_order_amount }} USD in your last order.'
    expect(subject.class.render(mailing, @subs1)).to eq('You spent 20.0 USD in your last order.')
  end

  it 'supports the subscriber_full_name variable' do
    mailing = Mailing.create store: @store, html: 'Hello {{ subscriber_full_name }}.'
    expect(subject.class.render(mailing, @subs1)).to eq('Hello Pedro Padron.')
    expect(subject.class.render(mailing, @subs2)).to eq('Hello Marcos Teixeira.')
  end

  it 'supports the store_name variable' do
    mailing = Mailing.create store: @store, html: 'Sent with love from {{ store_name }}'
    expect(subject.class.render(mailing, @subs1)).to eq('Sent with love from Test Store')
  end

  context 'when variable is empty' do
    it 'supports a fallback' do
      mailing   = Mailing.create store: @store, html: "Hello {{ subscriber_full_name | fallback: 'there' }}"
      anonymous = Subscriber.create store: @store, email: 'anonymous@example.com'

      expect(subject.class.render(mailing, anonymous)).to eq('Hello there')
    end
  end

  it 'passes the complete example from the smartrmail interview exercise' do
    template = <<-HTML
<html>
<body>
  <p> Hello {{ subscriber_full_name | fallback: 'there' }}!
    You spent {{ last_order_amount | fallback: '0' }} USD in your last order. </p>
    
  <p> Sent with love from {{ store_name }} </p>
</body>
</html>
HTML

    expected_output = <<-HTML
<html>
<body>
  <p> Hello there!
    You spent 0 USD in your last order. </p>
    
  <p> Sent with love from Test Store </p>
</body>
</html>
HTML

    mailing = Mailing.create store: @store, html: template
    subscriber = Subscriber.create store: @store, email: 'test@example.com'

    expect(subject.class.render(mailing, subscriber)).to eq(expected_output)
  end
end

RSpec.describe(Mailing) do
  it 'generates an array of hashes to be sent to the delivery API' do
    store   = Store.create name: 'Test Store', email: 'test@example.com'
    mailing = Mailing.create store: store, from: 'Test Store', subject: 'Hello', html: "Hello {{ subscriber_full_name | fallback: 'there' }}"

    subscriber1 = Subscriber.create store: @store, email: 'pgpadron@gmail.com',    first_name: 'Pedro',  last_name: 'Padron'
    subscriber2 = Subscriber.create store: @store, email: 'marcos@smartrmail.com', first_name: 'Marcos', last_name: 'Teixeira'
    subscriber3 = Subscriber.create store: @store, email: 'anonymous@example.com'

    expect(mailing.deliver_to([subscriber1, subscriber2, subscriber3])).to eq([
      { from: 'Test Store', subject: 'Hello', html: 'Hello Pedro Padron' },
      { from: 'Test Store', subject: 'Hello', html: 'Hello Marcos Teixeira' },
      { from: 'Test Store', subject: 'Hello', html: 'Hello there' }
    ])
  end
end