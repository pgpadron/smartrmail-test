require_relative '../config'

RSpec.configure do |config|
  require 'database_cleaner/active_record'
  DatabaseCleaner.strategy = :truncation
  config.after(:each) do
    DatabaseCleaner.clean
  end  
end