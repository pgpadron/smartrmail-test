class CreateSchema < ActiveRecord::Migration[6.0]
  def change
    create_table "stores", force: :cascade do |t|
      t.string "name"
      t.string "email"
    end
    
    create_table "mailings", force: :cascade do |t|
      t.string "from"
      t.text "subject"
      t.text "html"
      t.integer "store_id"
    end
    
    create_table "subscribers", force: :cascade do |t|
      t.string "email"
      t.string "first_name"
      t.string "last_name"
      t.integer "store_id"
    end
    
    create_table "orders", force: :cascade do |t|
      t.bigint "store_id"
      t.decimal "total", precision: 10, scale: 2
      t.string "subscriber_id"
    end
    
    create_table "products", force: :cascade do |t|
      t.bigint "store_id"
      t.string "title"
    end
    
    create_table "line_items", force: :cascade do |t|
      t.bigint "order_id"
      t.bigint "product_id"
      t.integer "quantity"
      t.decimal "price", precision: 10, scale: 2
    end
  end
end
