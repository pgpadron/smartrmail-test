class Mailing < ActiveRecord::Base
  belongs_to :store

  def deliver_to(subscribers=[])
    subscribers.map { |subscriber|
      {
        from: from,
        subject: subject,
        html: HtmlRenderer.render(self, subscriber)
      }
    }
  end
end