class Order < ActiveRecord::Base
  belongs_to :store
  belongs_to :subscriber
end