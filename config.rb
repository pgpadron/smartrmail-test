Bundler.require

APP_ENV = ['development', 'test'].include?(ENV['APP_ENV']) ? ENV['APP_ENV'] : 'development'

ActiveRecord::Base.establish_connection(
  adapter:  'sqlite3',
  database: 'db/data/' + APP_ENV + '.db'
)

require File.dirname(__FILE__) + '/helpers/renderer'
require File.dirname(__FILE__) + '/models/store'
require File.dirname(__FILE__) + '/models/subscriber'
require File.dirname(__FILE__) + '/models/product'
require File.dirname(__FILE__) + '/models/order'
require File.dirname(__FILE__) + '/models/mailing'
require File.dirname(__FILE__) + '/models/line_item'