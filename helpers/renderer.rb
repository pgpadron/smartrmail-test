class HtmlRenderer
  def self.render(mailing, subs)
    extraction = /{{(\s+)?(?<var>[a-zA-Z0-9_]*)(\s+)?(\|(\s+)?fallback:(\s+)?\'(?<fallback>.*?)\'(\s+)?)?}}/mi
    fallbacks  = Hash[mailing.html.scan(extraction)]

    merge_vars = {
      'subscriber_full_name' => [subs.first_name, ' ', subs.last_name].join.strip,
      'last_order_amount'    => subs&.orders&.last&.total,
      'store_name'           => subs&.store&.name
    }

    merge_vars.merge!(fallbacks) { |key, value, fallback|
      !value.to_s.empty? ? value.to_s : fallback
    }

    output = mailing.html

    merge_vars.each do |key, value|
      output = output.gsub(/{{(\s+)?#{key}.*?}}/, value.to_s)
    end

    return output
  end
end