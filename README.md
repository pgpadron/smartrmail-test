SmartrMail interview exercise
=============================

```sh
git clone https://gitlab.com/pgpadron/smartrmail-test.git
cd smartrmail-test
bundle install
APP_ENV=test bundle exec rspec
```

*Create the code that will receive the mailing object and an array of subscribers and return the hash that will be sent to the sending API.*

See `Mailing#deliver_to(subscribers=[])` in `models/mailing.rb`.

I have changed the resulting hash to include the `from` and `subject` fields and removed the `recipient_variables` because they were already replaced on our side and supposedly wouldn't be used by a remote API.